//@ts-check

function buildSelectorElement(query, smallest = true) {
  const $ = query.$
  let smallestSelector = ""
  let qualifiers = []
  let tagName = query.get(0).tagName
  let id = query.attr("id").value()
  if (id) qualifiers.push(`#${id}`)
  let classes = query.attr("class").value()
  let dotClasses = classes ? "." + classes.split(" ").join(".") : classes
  if (dotClasses) qualifiers.push(dotClasses)
  let attributes = query.attr().value()
  if (attributes.itemprop) qualifiers.push(`[itemprop=${attributes.itemprop}]`)
  if (attributes.name) qualifiers.push(`[name=${attributes.name}]`)
  if (query.index() === 0) qualifiers.push(":first-child")
  else if (query.index() === query.siblings().length) qualifiers.push(":last-child")
  else qualifiers.push(":nth-child(" + (query.index() + 1) + ")")

  const isUniqueSelector = (query, selector) =>
    $(query)
      .parent()
      .isUniqueSelector(selector)
      .value()

  smallestSelector = tagName
  let uniqueSelector = smallest ? isUniqueSelector(query, smallestSelector) : false
  let i = 0
  while (i < qualifiers.length && !uniqueSelector) {
    smallestSelector = `${smallestSelector}${qualifiers[i]}`
    uniqueSelector = smallest ? isUniqueSelector(query, smallestSelector) : false
    i++
  }
  return smallestSelector
}

const allButLast = arr => {
  arr.pop()
  return arr
}

function selectorPathBase(query, fullSelector = null, untilQuery = null) {
  const $ = query.$
  let parents = query.parents()
  if (!parents[0]) {
    query.set(":root")
    return query
  }

  const uniqueSelector = selector =>
    $.root()
      .isUniqueSelector(selector)
      .value()

  const matchUntilQuery = (node, untilQuery) => {
    let nodeSelector = node.uniqueSelector().value()
    let untilQuerySelector = untilQuery.uniqueSelector().value()
    return nodeSelector === untilQuerySelector
  }

  let selector = buildSelectorElement(query)
  let elementSelector
  let isUniqueSelector = fullSelector ? null : uniqueSelector(selector)
  let matchingUntilQuery = untilQuery ? matchUntilQuery($(selector), $(untilQuery)) : false

  let i = 0
  while (i < parents.length - 1 && (!isUniqueSelector && !fullSelector) && (untilQuery && !matchingUntilQuery)) {
    elementSelector = buildSelectorElement($(parents[i]))
    if (untilQuery) {
      matchingUntilQuery = matchUntilQuery($(elementSelector), $(untilQuery))
    } else {
      selector = elementSelector + " > " + selector
    }
    if (!fullSelector) {
      isUniqueSelector = $.root()
        .isUniqueSelector(selector)
        .value()
    }
    i++
  }
  query.set(selector)
  return query
}

// To export

function uniqueSelector(query) {
  return selectorPathBase(query)
}

function uniqueSelectorUntil(query, untilQuery) {
  return selectorPathBase(query, null, untilQuery)
}

function fullSelector(query) {
  return selectorPathBase(query, true)
}

function fullSelectorUntil(query, untilQuery) {
  return selectorPathBase(query, true, untilQuery)
}

function isUniqueSelector(query, selector) {
  if (selector) {
    let foundElements = query.find(selector)
    if (foundElements.length === 0) query.set(null)
    else query.set(foundElements.length === 1)
  } else {
    query.set(null)
  }
  return query
}

// unique selector without position in array
function selector(query) {
  let smallestUniqueSelector = query.uniqueSelector().value()
  if (smallestUniqueSelector.endsWith(":first-child")) query.set(allButLast(smallestUniqueSelector.split(":first-child")).join(""))
  else if (smallestUniqueSelector.endsWith(":last-child")) query.set(allButLast(smallestUniqueSelector.split(":last-child")).join(""))
  else if (smallestUniqueSelector.includes(":nth-child")) {
    let v = smallestUniqueSelector.split(/:nth-child\(\d+\)/)
    v = allButLast(v)
    v = v.join("")
    query.set(allButLast(smallestUniqueSelector.split(/:nth-child\(\d+\)/)).join(""))
  } else query.set(smallestUniqueSelector)
  return query
}

function parentListSelector(query, ...additionalQueries) {
  let nodeList
  let parents = query.parents()
  let commonParent
  const $ = query.$
  let parentIndex = 0

  for (let additionalQuery of additionalQueries) {
    let additionalQueryFound = false
    while (parents.length > 0 && !additionalQueryFound) {
      if ($(parents[parentIndex]).find($(additionalQuery))) {
        additionalQueryFound = true
        commonParent = $(parents[parentIndex])
      }
      parentIndex++
    }
  }

  let commonParents = $(commonParent).parents()
  if (!commonParents[0]) {
    query.set(null)
    return query
  }

  const isUniqueSelector = (query, selector) =>
    $(query)
      .parent()
      .isUniqueSelector(selector)
      .value()

  let isUnique = isUniqueSelector($(commonParent))
  if (!isUnique) nodeList = commonParent

  let i = 0
  while (i < commonParents.length - 1 && isUnique) {
    isUnique = isUniqueSelector($(commonParents[i]))
    if (!isUnique) nodeList = $(commonParents[i])
    i++
  }
  if (nodeList) {
    let selector = nodeList.selector().value()
    query.set(selector)
    return query._make(nodeList)
  }
  query.set(null)
  return query
}

module.exports = { selector, uniqueSelector, uniqueSelectorUntil, fullSelector, fullSelectorUntil, isUniqueSelector, parentListSelector }
