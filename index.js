const {
  selector,
  uniqueSelector,
  uniqueSelectorUntil,
  fullSelector,
  fullSelectorUntil,
  parentListSelector,
  isUniqueSelector
} = require("./selectors")

module.exports = {
  selector: {
    fn: selector
  },
  uniqueSelector: {
    alt: ["unqSelector"],
    fn: uniqueSelector
  },
  uniqueSelectorUntil: {
    alt: ["unqSelectorUntil"],
    fn: uniqueSelectorUntil
  },
  fullSelector: {
    fn: fullSelector
  },
  fullSelectorUntil: {
    fn: fullSelectorUntil
  },
  parentListSelector: {
    fn: parentListSelector
  },
  isUniqueSelector: {
    alt: ["isUnqSelector"],
    fn: isUniqueSelector
  }
}
